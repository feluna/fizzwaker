
/**
 * Module dependencies.
 */

var express = require('express');
var routes = require('./routes');
var user = require('./routes/user');
var http = require('http');
var path = require('path');

var app = express();


var schedule 		= require('node-schedule');
var request 		= require('request');

// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.methodOverride());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

app.get('/', routes.index);
app.get('/users', user.list);

http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});

var urls = [
	'http://fizzapp.herokuapp.com/getInitialTweets',
	'http://journifyapp.herokuapp.com/auth',
	'http://journifyappdev.herokuapp.com/auth',
	'http://journifysocial.herokuapp.com/auth',
	'http://journifysocialdev.herokuapp.com/auth',
	'http://socialmediatest.herokuapp.com/',
	'http://journifybackgroundmanager.herokuapp.com/asd'
];

function sendRequest() {
	for (var i = 0; i < urls.length; i++) {
		request.get(urls[i] , function(err , resp , body) {
			console.log(body);
		});
	}	
};

var rule  = new schedule.RecurrenceRule();
var rule2 = new schedule.RecurrenceRule();

rule.minute  = 01;
rule2.minute = 31;

schedule.scheduleJob(rule  , sendRequest);
schedule.scheduleJob(rule2 , sendRequest);


